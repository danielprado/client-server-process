"""
This script contains method that let you
get info about
- CPU Name (import cpuinfo)
- OS Name and version (import platform)
- Get OS logged-in user name (import getpass)
- A list of process running (import psutil)
- Current user IP (import socket)
And send this info as a JSON payload to specified API endpoint
using "argparse" to set the endpoint and "request" to send and
store the data.

NOTE: Execute this script with "--url" argument
"""

import platform
import cpuinfo
import getpass
import psutil
import socket
import requests
import argparse
import json


def get_cpu():
    """
    Get CPU name
    :return: string
    """
    name = cpuinfo.get_cpu_info()['brand_raw']
    print('Getting CPU name: ' + name)
    return name


def get_os_name():
    """
    Get OS name and version
    :return: string
    """
    os_name = platform.platform()
    print('Getting OS name: ' + os_name)
    return os_name


def get_list_of_process_sorted_by_memory():
    """
    Get list of running process sorted by Memory Usage
    :return: Dict
    """
    list_of_proc_objects = []
    # Iterate over the list
    for proc in psutil.process_iter():
        try:
            # Fetch process details as dict
            pinfo = proc.as_dict(attrs=['pid', 'name', 'username'])
            pinfo['vms'] = proc.memory_info().vms / (1024 * 1024)
            # Append dict to list
            list_of_proc_objects.append(pinfo);
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    # Sort list of dict by key vms i.e. memory usage
    list_of_proc_objects = sorted(list_of_proc_objects, key=lambda proc_obj: proc_obj['vms'], reverse=True)
    print(f'Getting current process: {len(list_of_proc_objects)} running...')
    return list_of_proc_objects


def extract_ip():
    """
    Get current server IP
    :return: string
    """
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        st.connect(('10.255.255.255', 1))
        IP = st.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        st.close()
    print(f'Getting current IP: {IP}')
    return IP


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send server info to specified endpoint.')
    parser.add_argument('--url',
                        default=None,
                        help='Set API endpoint to send and store data on database')
    args = parser.parse_args()
    payload = {
        'logged_user': getpass.getuser(),
        'os_name': get_os_name(),
        'cpu': get_cpu(),
        'ip': extract_ip(),
        'process': get_list_of_process_sorted_by_memory(),
    }
    if not args.url:
        print('Please set --url param to continue, file generated.')
        with open('data.json', 'w') as f:
            json.dump(payload, f)
    else:
        print(f'Creating and sending data to {args.url}.')
        url = args.url

        r = requests.post(
            url=url,
            json=payload,
            headers={
                'Accept': 'application/json'
            }
        )

        if r.status_code == 200:
            print('Data was sent successfully to server')
            print(r.content)
        else:
            print('An error was occurred please check connection or params:')
            print(r.content)
