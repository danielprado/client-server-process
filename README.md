## Instructions

Clone the repository

```sh
  git clone https://gitlab.com/danielprado/client-server-process.git client
```

Execute this command to install and execute script

```sh
  cd client # The folder project
  pip install -r requirements.txt
  python main.py --url http://endpoint-to-save-sata
```

On Windows generate ```.exe``` file
```sh
  pyinstaller --onefile --console main.py
```

It creates a folder ```dist``` with an ``.exe``` file and then execute ```cmd``` on folder

```sh
  main.exe --url http://endpoint-to-save-sata
```
